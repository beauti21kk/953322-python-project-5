@startuml
skinparam monochrome true
skinparam ClassBackgroundColor White
hide empty members
hide circle
skinparam defaultFontName Arial
skinparam defaultFontSize 11
left to right direction
class "<b>Neglecting to authorize after authentication : Decision</b>" as __1_Neglecting_to_authorize_after_authentication <<Multiple Answers>> 
class "<b>Periodically perform authorization as an explicit</b>\n<b>check. : Design Solution</b>" as __2_Periodically_perform_authorization_as_an_explicit_check_ {
background reading = "s01"
}

class "<b>Adopt automated testing. : Design Solution</b>" as __3_Adopt_automated_testing_ {
background reading = "s04"
}

class "<b> Implement strong authentication methods such</b>\n<b>as multi-factor authentication (MFA) to reduce</b>\n<b>the risk of unauthorized access. : Design Solution</b>" as __4__Implement_strong_authentication_methods_such_as_multi_factor_authentication__MFA__to_reduce_the_risk_of_unauthorized_access_ {
background reading = "s24"
}

class "<b>Implementing a framework will help you avoid the</b>\n<b>web security risks caused by faulty authentication.</b>\n<b>: Design Solution</b>" as __5_Implementing_a_framework_will_help_you_avoid_the_web_security_risks_caused_by_faulty_authentication_ {
background reading = "s24"
}

class "<b>Use OAuth2 for single sign on (SSO) with OpenID</b>\n<b>Connect. : Design Solution</b>" as __6_Use_OAuth2_for_single_sign_on__SSO__with_OpenID_Connect_ {
background reading = "s26"
}

class "<b>Add more Authentication ex. Multi-factor authentication,</b>\n<b>Certificate-based authentication, Biometric authentication,</b>\n<b>and Token-based authentication. : Design Solution</b>" as __7_Add_more_Authentication_ex__Multi_factor_authentication__Certificate_based_authentication__Biometric_authentication__and_Token_based_authentication_ {
background reading = "s27"
}

class "<b>Lack of defense in depth : Decision</b>" as __8_Lack_of_defense_in_depth <<Single Answers>> 
class "<b>Implement multiple layers of security defenses</b>\n<b>to reduce the likelihood of a successful attack.</b>\n<b>: Design Solution</b>" as __9_Implement_multiple_layers_of_security_defenses_to_reduce_the_likelihood_of_a_successful_attack_ {
background reading = "s18"
}

class "<b>Man-in-the-middle (MitM) attacks : Decision</b>" as __10_Man_in_the_middle__MitM__attacks <<Single Answers>> 
class "<b>Properly scope permissions across users and machines.</b>\n<b>: Design Solution</b>" as __11_Properly_scope_permissions_across_users_and_machines_ {
background reading = "s32"
}

class "<b>an unauthorized entity having access to a system</b>\n<b>or service that it should not : Decision</b>" as __12_an_unauthorized_entity_having_access_to_a_system_or_service_that_it_should_not <<Multiple Answers>> 
class "<b>Implement Principle of Least Privilege. : Design</b>\n<b>Solution</b>" as __13_Implement_Principle_of_Least_Privilege_ {
background reading = "s01"
}

class "<b>Train employees on cyber security best practices.</b>\n<b>: Design Solution</b>" as __14_Train_employees_on_cyber_security_best_practices_ {
background reading = "s02"
}

class "<b>Architecture analysis assessments. : Design Solution</b>" as __15_Architecture_analysis_assessments_ {
background reading = "s10"
}

__1_Neglecting_to_authorize_after_authentication --> __2_Periodically_perform_authorization_as_an_explicit_check_: <<Option>>\n{name = "Solution 1"}
__1_Neglecting_to_authorize_after_authentication --> __3_Adopt_automated_testing_: <<Option>>\n{name = "Solution 2"}
__1_Neglecting_to_authorize_after_authentication --> __4__Implement_strong_authentication_methods_such_as_multi_factor_authentication__MFA__to_reduce_the_risk_of_unauthorized_access_: <<Option>>\n{name = "Solution 3"}
__1_Neglecting_to_authorize_after_authentication --> __5_Implementing_a_framework_will_help_you_avoid_the_web_security_risks_caused_by_faulty_authentication_: <<Option>>\n{name = "Solution 4"}
__1_Neglecting_to_authorize_after_authentication --> __6_Use_OAuth2_for_single_sign_on__SSO__with_OpenID_Connect_: <<Option>>\n{name = "Solution 5"}
__1_Neglecting_to_authorize_after_authentication --> __7_Add_more_Authentication_ex__Multi_factor_authentication__Certificate_based_authentication__Biometric_authentication__and_Token_based_authentication_: <<Option>>\n{name = "Solution 6"}
__3_Adopt_automated_testing_ --> __8_Lack_of_defense_in_depth
__4__Implement_strong_authentication_methods_such_as_multi_factor_authentication__MFA__to_reduce_the_risk_of_unauthorized_access_ --> __10_Man_in_the_middle__MitM__attacks
__7_Add_more_Authentication_ex__Multi_factor_authentication__Certificate_based_authentication__Biometric_authentication__and_Token_based_authentication_ --> __12_an_unauthorized_entity_having_access_to_a_system_or_service_that_it_should_not
__8_Lack_of_defense_in_depth --> __9_Implement_multiple_layers_of_security_defenses_to_reduce_the_likelihood_of_a_successful_attack_: <<Option>>\n{name = "Solution 1"}
__10_Man_in_the_middle__MitM__attacks --> __11_Properly_scope_permissions_across_users_and_machines_: <<Option>>\n{name = "Solution 1"}
__12_an_unauthorized_entity_having_access_to_a_system_or_service_that_it_should_not --> __13_Implement_Principle_of_Least_Privilege_: <<Option>>\n{name = "Solution 1"}
__12_an_unauthorized_entity_having_access_to_a_system_or_service_that_it_should_not --> __14_Train_employees_on_cyber_security_best_practices_: <<Option>>\n{name = "Solution 2"}
__12_an_unauthorized_entity_having_access_to_a_system_or_service_that_it_should_not --> __15_Architecture_analysis_assessments_: <<Option>>\n{name = "Solution 3"}
@enduml
