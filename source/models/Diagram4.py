from metamodels.guidance_metamodel import decision, design_solution, add_decision_option_link, single_answer, practice, multiple_answers
from codeable_models import CClass, add_links, CBundle

#Problem 2
problem_2 = CClass(decision, "SQL Injection", stereotype_instances=multiple_answers)

solution_1_2 = CClass(design_solution, "Using parameterized queries instead of dynamic SQL statements.",
                    values={"background reading": "s02"})
solution_2_2 = CClass(design_solution, "Using a web application firewall.",
                    values={"background reading": "s02"})
solution_3_2 = CClass(design_solution, "Don’t use dynamic SQL.",
                    values={"background reading": "s02"})

add_decision_option_link(problem_2, solution_1_2, "Solution 1")
add_decision_option_link(problem_2, solution_2_2, "Solution 2")
add_decision_option_link(problem_2, solution_3_2, "Solution 3")

#Problem 6
problem_6 = CClass(decision, "Poor key management", stereotype_instances=multiple_answers)

solution_1_6 = CClass(design_solution, "Strict policy-based controls to prevent the misuse/reuse of keys.",
                    values={"background reading": "s06"})
solution_2_6 = CClass(design_solution, "Secure coding guidelines and standards.",
                    values={"background reading": "s06"})

add_decision_option_link(problem_6, solution_1_6, "Solution 1")
add_decision_option_link(problem_6, solution_2_6, "Solution 2")

#Problem 9
problem_9 = CClass(decision, "Insecure APIs", stereotype_instances=multiple_answers)

solution_1_9 = CClass(design_solution, "Establishing a policy on open-source use.",
                    values={"background reading": "s09"})
solution_2_9 = CClass(design_solution, "Scan for vulnerabilities regularly.",
                    values={"background reading": "s09"})
solution_3_9 = CClass(design_solution, "Code review involves reviewing the code written by developers to identify potential security issues.",
                    values={"background reading": "s09"})
solution_4_9 = CClass(design_solution, "Reducing misconfigurations and monitoring for security issues.",
                    values={"background reading": "s09"})
solution_5_9 = CClass(design_solution, "Use the services that focus on checking codebase dependencies.",
                    values={"background reading": "s09"})
solution_6_9 = CClass(design_solution, "Integrating a scanning tool like Software Composition Analysis.",
                    values={"background reading": "s09"})
solution_7_9 = CClass(design_solution, "Implement multi-factor authentication.",
                    values={"background reading": "s09"})

add_decision_option_link(problem_9, solution_1_9, "Solution 1")
add_decision_option_link(problem_9, solution_2_9, "Solution 2")
add_decision_option_link(problem_9, solution_3_9, "Solution 3")
add_decision_option_link(problem_9, solution_4_9, "Solution 4")
add_decision_option_link(problem_9, solution_5_9, "Solution 5")
add_decision_option_link(problem_9, solution_6_9, "Solution 6")
add_decision_option_link(problem_9, solution_7_9, "Solution 7")

#Problem 38
problem_38 = CClass(decision, "Unpatched vulnerabilities", stereotype_instances=multiple_answers)

solution_1_38 = CClass(design_solution, "Change to HTTPS, That provides protection against these vulnerabilities by encrypting all exchanges between a web browser and web server. As a result, HTTPS ensures that no one can tamper with these transactions, thus securing users' privacy and preventing sensitive information from falling into the wrong hands.",
                    values={"background reading": "s38"})
solution_2_38 = CClass(design_solution, "Train employees on cyber security best practices.",
                    values={"background reading": "s38"})

add_decision_option_link(problem_38, solution_1_38, "Solution 1")
add_decision_option_link(problem_38, solution_2_38, "Solution 2")

#Problem 23
problem_23 = CClass(decision, "Fault is irrelevant", stereotype_instances=multiple_answers)

solution_1_23 = CClass(design_solution, "Security teams should focus on vulnerabilities by evaluating their scope, managing the risks.",
                    values={"background reading": "s23"})
solution_2_23 = CClass(design_solution, "Running automatic test cases to detect difficult risks.",
                    values={"background reading": "s23"})

add_decision_option_link(problem_23, solution_1_23, "Solution 1")
add_decision_option_link(problem_23, solution_2_23, "Solution 2")

#Problem 14
problem_14 = CClass(decision, "Poorly written code", stereotype_instances=multiple_answers)

solution_1_14 = CClass(design_solution, "Treat Software Security as a Priority Right From The Start.",
                    values={"background reading": "s14"})
solution_2_14 = CClass(design_solution, "Conduct testing to identify and address coding bugs. Implement secure coding practices and perform regular code reviews to minimize the introduction of vulnerabilities.",
                    values={"background reading": "s14"})
solution_3_14 = CClass(design_solution, "Maintain an OSS Security Library Inventory.",
                    values={"background reading": "s14"})
solution_4_14 = CClass(design_solution, "Verify that the code is efficient, scalable and secure.",
                    values={"background reading": "s14"})

add_decision_option_link(problem_23, solution_1_14, "Solution 1")
add_decision_option_link(problem_23, solution_2_14, "Solution 2")
add_decision_option_link(problem_23, solution_3_14, "Solution 3")
add_decision_option_link(problem_23, solution_4_14, "Solution 4")

#Problem 17
problem_17 = CClass(decision, "Unfettered access to source code repositories and CI/CD pipelines", stereotype_instances=multiple_answers)

solution_1_17 = CClass(design_solution, "Implement automated security testing, avoid access to repositories, and apply the principle of least privilege.",
                    values={"background reading": "s17"})
solution_2_17 = CClass(design_solution, "Set limitations or privacy to access source code.",
                    values={"background reading": "s17"})
solution_3_17 = CClass(design_solution, "Use static analytical methods to design and test components.",
                    values={"background reading": "s17"})

add_decision_option_link(problem_17, solution_1_17, "Solution 1")
add_decision_option_link(problem_17, solution_2_17, "Solution 2")
add_decision_option_link(problem_17, solution_3_17, "Solution 3")

#Problem 15
problem_15 = CClass(decision, "Security misconfiguration", stereotype_instances=single_answer)

solution_1_15 = CClass(design_solution, "Include application security in data privacy compliance strategy.",
                    values={"background reading": "s15"})

add_decision_option_link(problem_15, solution_1_15, "Solution 1")

#Problem 1
problem_1 = CClass(decision, "Incorrect trust assumptions", stereotype_instances=single_answer)

solution_1_1 = CClass(design_solution, "Make sure all data from an untrusted client are validated.",
                    values={"background reading": "s01"})

add_decision_option_link(problem_1, solution_1_1, "Solution 1")

#Problem 35
problem_35 = CClass(decision, "Unsanitized Input", stereotype_instances=single_answer)

solution_1_35 = CClass(design_solution, "Avoid placing user-provided input directly into SQL statements.",
                    values={"background reading": "s35"})

add_decision_option_link(problem_35, solution_1_35, "Solution 1")

#Problem 16
problem_16 = CClass(decision, "Unsanitized Input", stereotype_instances=multiple_answers)

solution_1_16 = CClass(design_solution, "dedicated time to focus on security awareness education and security protocol training.",
                    values={"background reading": "s16"})
solution_2_16 = CClass(design_solution, "Implement Principle of Least Privilege.",
                    values={"background reading": "s16"})
solution_3_16 = CClass(design_solution, "Continuous monitoring helps in detecting and responding to security incidents in real-time.",
                    values={"background reading": "s16"})
solution_4_16 = CClass(design_solution, "User activity tracking, file integrity monitoring, and network activity logs are examples of monitoring and auditing practices.",
                    values={"background reading": "s16"})
solution_5_16 = CClass(design_solution, "Use security testing.",
                    values={"background reading": "s16"})

add_decision_option_link(problem_16, solution_1_16, "Solution 1")
add_decision_option_link(problem_16, solution_2_16, "Solution 2")
add_decision_option_link(problem_16, solution_3_16, "Solution 3")
add_decision_option_link(problem_16, solution_4_16, "Solution 4")
add_decision_option_link(problem_16, solution_5_16, "Solution 5")

#Problem 20
problem_20 = CClass(decision, "Ransomware Attacks", stereotype_instances=single_answer)

solution_1_20 = CClass(design_solution, "Prioritize cybersecurity more in budgeting.",
                    values={"background reading": "s20"})

add_decision_option_link(problem_20, solution_1_20, "Solution 1")

#Link Diagram
add_links({solution_2_2: problem_6}, role_name="next decision")
add_links({solution_2_2: problem_9}, role_name="next decision")
add_links({solution_1_9: problem_38}, role_name="next decision")
add_links({solution_2_9: problem_23}, role_name="next decision")
add_links({solution_3_9: problem_14}, role_name="next decision")
add_links({solution_4_9: problem_15}, role_name="next decision")
add_links({solution_5_9: problem_1}, role_name="next decision")
add_links({solution_1_23: problem_38}, role_name="next decision")
add_links({solution_1_38: problem_20}, role_name="next decision")
add_links({solution_2_14: problem_17}, role_name="next decision")
add_links({solution_1_1: problem_35}, role_name="next decision")
add_links({solution_1_1: problem_16}, role_name="next decision")

_all = CBundle("Diagram4",
                    elements=[problem_16, solution_1_16, solution_2_16,solution_3_16,solution_4_16,solution_5_16,
                              problem_20, solution_1_20,
                              problem_1, solution_1_1,
                              problem_14, solution_1_14, solution_2_14, solution_3_14, solution_4_14,
                              problem_15, solution_1_15,
                              problem_17, solution_1_17, solution_2_17, solution_3_17,
                              problem_9, solution_1_9, solution_2_9, solution_3_9, solution_4_9, solution_5_9 ,solution_6_9, solution_7_9,
                              problem_2, solution_1_2, solution_2_2, solution_3_2,
                              problem_6, solution_1_6, solution_2_6,
                              problem_23, solution_1_23, solution_2_23,
                              problem_35, solution_1_35,
                              problem_38, solution_1_38, solution_2_38])

Diagram4 = [_all, {}]
