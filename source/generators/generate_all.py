# from plant_uml_renderer import PlantUMLGenerator
import sys   
sys.path.append('.')
from plant_uml_renderer import PlantUMLGenerator
from source.models.Diagram1 import Diagram1
from source.models.Diagram4 import Diagram4
from source.models.Diagram2 import Diagram2
from source.models.Diagram3 import Diagram3

# UMLgenerator
generator = PlantUMLGenerator(delete_gen_dir_during_init=True)
generator.object_model_renderer.name_break_length = 45
generator.object_model_renderer.left_to_right = True
generator.directory = "./generated_output"
generator.plant_uml_jar_path = "./libs/plantuml.jar"

class_models = {'ADD': Diagram1}
for key, value in class_models.items():
    generator.generate_object_models(key, value)

class_models = {'ADD': Diagram2}
for key, value in class_models.items():
    generator.generate_object_models(key, value)

class_models = {'ADD': Diagram3}
for key, value in class_models.items():
    generator.generate_object_models(key, value)

class_models = {'ADD': Diagram4}
for key, value in class_models.items():
    generator.generate_object_models(key, value)
