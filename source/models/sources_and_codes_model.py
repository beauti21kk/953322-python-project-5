from codeable_models import CClass, add_links
from metamodels.GT_metamodel import source
from models.Diagram1 import *
from models.Diagram2 import *
from models.Diagram3 import *
from models.Diagram4 import *
# ### Sources ###
problem_1_S01 = CClass(source, "problem_1-S01", values={
    "title": "Avoiding top 10 software security design flaws",
    "url": "https://ieeecs-media.computer.org/media/technical-activities/CYBSI/docs/Top-10-Flaws.pdf",
    "archive url": "https://web.archive.org/web/20230702031229/https://ieeecs-media.computer.org/media/technical-activities/CYBSI/docs/Top-10-Flaws.pdf",
    "date": "N/A",
    "author type": "IEEE Computer Society",
    "type": "PDF file"})
s01_codes = [problem_1]
add_links({problem_1_S01: s01_codes}, role_name="contained_code")

problem_2_S02 = CClass(source, "problem_2-S02", values={
    "title": "10 cyber security risks in software development and how to mitigate them",
    "url": "https://devtalents.com/cyber-security-during-software-development/",
    "archive url": "https://web.archive.org/web/20230703032931/https://devtalents.com/cyber-security-during-software-development/",
    "date": "10.02.2023",
    "author type": "Olga Trąd",
    "type": "Blog Post"})
s02_codes = [problem_2]
add_links({problem_2_S02: s02_codes}, role_name="contained_code")

problem_3_S03 = CClass(source, "problem_3-S03", values={
    "title": "10 Agile Software Development Security Concerns You Need to Know",
    "url": "https://www.legitsecurity.com/blog/10-agile-software-development-security-concerns-you-need-to-know",
    "archive url": "https://web.archive.org/web/20230703033943/https://www.legitsecurity.com/blog/10-agile-software-development-security-concerns-you-need-to-know",
    "date": "N/A",
    "author type": "Alex Babar",
    "type": "Blog Post"})
s03_codes = [problem_3]
add_links({problem_3_S03: s03_codes}, role_name="contained_code")

problem_4_S04 = CClass(source, "problem_4-S04", values={
    "title": "Application Security 101",
    "url": "https://www.trendmicro.com/vinfo/us/security/news/virtualization-and-cloud/application-security-101",
    "archive url": "https://web.archive.org/web/20230703033943/https://www.legitsecurity.com/blog/10-agile-software-development-security-concerns-you-need-to-know",
    "date": "27.07.2020",
    "author type": "N/A",
    "type": "Blog Post"})
s04_codes = [problem_4]
add_links({problem_4_S04: s04_codes}, role_name="contained_code")

problem_6_S06 = CClass(source, "problem_6-S06", values={
    "title": "Common Software Vulnerabilities in 2022 and Ways to Prevent Them",
    "url": "https://codesigningstore.com/common-software-vulnerabilities",
    "archive url": "https://web.archive.org/web/20230703033943/https://www.legitsecurity.com/blog/10-agile-software-development-security-concerns-you-need-to-know",
    "date": "2022",
    "author type": "N/A",
    "type": "Blog Post"})
s06_codes = [problem_6]
add_links({problem_6_S06: s06_codes}, role_name="contained_code")

problem_9_S09 = CClass(source, "problem_9-S09", values={
    "title": "Most Extensive Cyber Security Challenges & Solutions in 2023",
    "url": "https://www.knowledgehut.com/blog/security/cyber-security-challenges",
    "archive url": "https://tinyurl.com/973fxzbb",
    "date": "19.06.2023",
    "author type": "Blog Author",
    "type": "Article"})
s09_codes = [problem_9]
add_links({problem_9_S09: s09_codes}, role_name="contained_code")

problem_10_S10 = CClass(source, "problem_10-S10", values={
    "title": "3 security risks that architecture analysis can resolve",
    "url": "https://www.synopsys.com/blogs/software-security/security-risks-that-architecture-analysis-can-resolve/",
    "archive url": "https://tinyurl.com/bdcuw9hj",
    "date": "25.01.2016",
    "author type": "Team of writer",
    "type": "Article"})
s10_codes = [problem_10]
add_links({problem_10_S10: s10_codes}, role_name="contained_code")

problem_11_S11 = CClass(source, "problem_11-S11", values={
    "title": "Common types of security vulnerabilities & ways to fix them",
    "url": "https://itrexgroup.com/blog/security-vulnerability-types-and-ways-to-fix-them/#",
    "archive url": "https://tinyurl.com/4vb4pvak",
    "date": "06.07.2022",
    "author type": "Innovation Analyst",
    "type": "Article"})
s11_codes = [problem_11]
add_links({problem_11_S11: s11_codes}, role_name="contained_code")

problem_12_S12 = CClass(source, "problem_12-S12", values={
    "title": "Best Practices For Secure Software Development",
    "url": "https://web.archive.org/web/20230702170042/https://www.perforce.com/blog/sca/best-practices-secure-software-development",
    "archive url": "https://tinyurl.com/4vzcy6rv",
    "date": "31.03.2023",
    "author type": "Senior Solution Architect",
    "type": "Article"})
s12_codes = [problem_12]
add_links({problem_12_S12: s12_codes}, role_name="contained_code")

problem_13_S13 = CClass(source, "problem_13-S13", values={
    "title": "6 security risks in software development and how to address them",
    "url": "https://www.infoworld.com/article/3607914/6-security-risks-in-software-development-and-how-to-address-them.html",
    "archive url": "/web/20230702162855/https://www.infoworld.com/article/3607914/6-security-risks-in-software-development-and-how-to-address-them.html",
    "date": "08.03.2021",
    "author type": "Technology Blogger",
    "type": "Blog Post"})
s13_codes = [problem_13]
add_links({problem_13_S13: s13_codes}, role_name="contained_code")

problem_14_S14 = CClass(source, "problem_14-S14", values={
    "title": "Six security challenges — and how to overcome them",
    "url": "https://www.rackspace.com/blog/six-security-challenges",
    "archive url": "/web/20230702171838/https://www.rackspace.com/blog/six-security-challenges",
    "date": "24.08.2021",
    "author type": "Technology Blogger",
    "type": "Blog Post"})
s14_codes = [problem_14]
add_links({problem_14_S14: s14_codes}, role_name="contained_code")

problem_15_S15 = CClass(source, "problem_15-S15", values={
    "title": "A Catalog of Security Architecture Weaknesses",
    "url": "https://s2e-lab.github.io/preprints/icsaw17-preprint.pdf",
    "archive url": "/web/20230702174324/https://s2e-lab.github.io/preprints/icsaw17-preprint.pdf",
    "date": "26.06.2017",
    "author type": "Researcher",
    "type": "Conference Paper"})
s15_codes = [problem_15]
add_links({problem_15_S15: s15_codes}, role_name="contained_code")

problem_16_S16 = CClass(source, "problem_16-S16", values={
    "title": "Not having a security architecture",
    "url": "https://www.kambda.com/main-software-architecture-issues-and-challenges/",
    "archive url": "/web/20230702180659/https://www.networkworld.com/article/2305984/not-having-a-security-architecture.html",
    "date": "28.08.2006",
    "author type": "Technology Blogger",
    "type": "Blog Post"})
s16_codes = [problem_16]
add_links({problem_16_S16: s16_codes}, role_name="contained_code")

problem_17_S17 = CClass(source, "problem_17-S17", values={
    "title": "Security Risks in the Software Development Lifecycle",
    "url": "https://www.researchgate.net/publication/369538781_Security_Risks_in_the_Software_Development_Lifecycle_A_Review",
    "archive url": "/web/20230702182257/https://www.researchgate.net/publication/369538781_Security_Risks_in_the_Software_Development_Lifecycle_A_Review",
    "date": "23.02.2023",
    "author type": "Researcher",
    "type": "Technical Report"})
s17_codes = [problem_17]
add_links({problem_17_S17: s17_codes}, role_name="contained_code")

problem_19_S19 = CClass(source, "problem_19-S19", values={
    "title": "10 BEST PRACTICES FOR SOFTWARE DEVELOPMENT SECURITY",
    "url": "https://www.orientsoftware.com/blog/software-development-security/",
    "archive url": "https://tinyurl.com/yb9shzzy",
    "date": "29.11.2021",
    "author type": "Øyvind Forsbak",
    "type": "Blog Post"})
s19_codes = [problem_19]
add_links({problem_19_S19: s19_codes}, role_name="contained_code")

problem_20_S20 = CClass(source, "problem_20-S20", values={
    "title": "5 Key Open Source Security Risks and How to Prevent Them",
    "url": "https://blog.sonatype.com/5-key-open-source-security-risks-and-how-to-prevent-them",
    "archive url": "https://tinyurl.com/2pm69bkn",
    "date": "01.12.2022",
    "author type": "Luke Mcbride",
    "type": "Blog Post"})
s20_codes = [problem_20]
add_links({problem_20_S20: s20_codes}, role_name="contained_code")

problem_23_S23 = CClass(source, "problem_23-S23", values={
    "title": "10 Agile Software Development Security Concerns You Need to Know",
    "url": "https://www.legitsecurity.com/blog/10-agile-software-development-security-concerns-you-need-to-know",
    "archive url": "https://tinyurl.com/mucett5d",
    "date": "31.08.2022",
    "author type": "Alex Babar",
    "type": "Blog Post"})
s23_codes = [problem_23]
add_links({problem_23_S23: s23_codes}, role_name="contained_code")

problem_24_S24 = CClass(source, "problem_24-S24", values={
    "title": "10 Common Web Security Vulnerabilities",
    "url": "https://www.toptal.com/cyber-security/10-most-common-web-security-vulnerabilities",
    "archive url": "https://tinyurl.com/bdh9taef",
    "date": "04.04.2016",
    "author type": "Gergely Kalman",
    "type": "Blog Post"})
s24_codes = [problem_24]
add_links({problem_24_S24: s24_codes}, role_name="contained_code")

problem_28_S28 = CClass(source, "problem_28-S28", values={
    "title": "Session Management",
    "url": "https://cheatsheetseries.owasp.org/cheatsheets/Session_Management_Cheat_Sheet.html",
    "archive url": "https://tinyurl.com/nn8f5e3n",
    "date": "16.07.2019",
    "author type": "OWASP",
    "type": "Sheet"})
s28_codes = [problem_28]
add_links({problem_28_S28: s28_codes}, role_name="contained_code")

problem_31_S31 = CClass(source, "problem_31-S31", values={
    "title": "4 Common Security Issues Found In Password-Based Login",
    "url": "https://www.loginradius.com/blog/identity/common-vulnerabilities-password-based-login/",
    "archive url": "https://web.archive.org/web/20220701072922/https://www.loginradius.com/blog/identity/common-vulnerabilities-password-based-login/",
    "date": "N/A",
    "author type": "Srishti Singh",
    "type": "Blog Post"})
s31_codes = [problem_31]
add_links({problem_31_S31: s31_codes}, role_name="contained_code")

problem_32_S32 = CClass(source, "problem_32-S32", values={
    "title": "COMMON CLOUD THREATS:CREDENTIAL THEFT",
    "url": "https://www.crowdstrike.com/cybersecurity-101/cloud-security/credential-theft/",
    "archive url": "https://web.archive.org/web/20230610040043/https://www.crowdstrike.com/cybersecurity-101/cloud-security/credential-theft/",
    "date": "12.01.2023",
    "author type": "David Puzas",
    "type": "Blog Post"})
s32_codes = [problem_32]
add_links({problem_32_S32: s32_codes}, role_name="contained_code")

problem_33_S33 = CClass(source, "problem_33-S33", values={
    "title": "Vulnerabilities in password-based login",
    "url": "https://web.archive.org/web/20230521093349/https://portswigger.net/web-security/authentication/password-based",
    "archive url": "https://tinyurl.com/4vb4pvak",
    "date": "N/A",
    "author type": "portswigger",
    "type": "Blog Post"})
s33_codes = [problem_33]
add_links({problem_33_S33: s33_codes}, role_name="contained_code")

problem_35_S35 = CClass(source, "problem_35-S35", values={
    "title": "Secure Communication Principles & Tips By Salt Communications",
    "url": "https://saltcommunications.com/press-releases/secure-communication-principles-tips-by-salt-communications/",
    "archive url": "https://web.archive.org/web/20230321132543/https://saltcommunications.com/press-releases/secure-communication-principles-tips-by-salt-communications/",
    "date": "15.08.2022",
    "author type": "Nicole Allen",
    "type": "Blog Post"})
s35_codes = [problem_35]
add_links({problem_35_S35: s35_codes}, role_name="contained_code")

problem_38_S38 = CClass(source, "problem_38-S38", values={
    "title": "Engineering Solutions to Security Issues",
    "url": "https://thenewstack.io/engineering-solutions-to-security-issues/",
    "archive url": "https://web.archive.org/web/*/https://thenewstack.io/engineering-solutions-to-security-issues/*",
    "date": "24.05.2021",
    "author type": "Ron Powell",
    "type": "Blog Post"})
s38_codes = [problem_38]
add_links({problem_38_S38: s38_codes}, role_name="contained_code")

problem_39_S39 = CClass(source, "problem_39-S39", values={
    "title": "4 Common Software Security Development Issues & How to Fix Them",
    "url": "https://devm.io/security/security-dev-issues-174540",
    "archive url": "https://web.archive.org/web/*/https://devm.io/security/security-dev-issues-174540*",
    "date": "02.07.2021",
    "author type": "Tim Jarrett",
    "type": "Blog Post"})
s39_codes = [problem_39]
add_links({problem_39_S39: s39_codes}, role_name="contained_code")

problem_40_S40 = CClass(source, "problem_40-S40", values={
    "title": "10 cyber security risks in software development and how to mitigate them",
    "url": "https://devtalents.com/cyber-security-during-software-development/",
    "archive url": "https://web.archive.org/web/*/https://devtalents.com/cyber-security-during-software-development/*",
    "date": "02.10.2023",
    "author type": "Olga Trąd",
    "type": "Blog Post"})
s40_codes = [problem_40]
add_links({problem_40_S40: s40_codes}, role_name="contained_code")

problem_42_S42 = CClass(source, "problem_42-S42", values={
    "title": "Top Risks In Software Development Life Cycle: 7-Minute Read",
    "url": "https://cystack.net/blog/security-risks-in-software-development-life-cycle",
    "archive url": "https://web.archive.org/web/20230701000000*/https://cystack.net/blog/security-risks-in-software-development-life-cycle",
    "date": "05.04.23",
    "author type": "Jenny Duong",
    "type": "Blog Post"})
s42_codes = [problem_42]
add_links({problem_42_S42: s42_codes}, role_name="contained_code")
