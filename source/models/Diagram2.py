from metamodels.guidance_metamodel import decision, design_solution, add_decision_option_link, single_answer, practice, multiple_answers
from codeable_models import CClass, add_links, CBundle


problem_33 = CClass(
    decision, "Brute-force attacks", stereotype_instances=single_answer)

# Create Solution 1 for Problem 33
solution_1_33 = CClass(design_solution, "Locking the account that the remote user is trying to access if they make too many failed login attempts . Blocking the remote user's IP address if they make too many login attempts in quick succession",
                    values={"background reading": "s33"})

# Link Problem 33 to Solution 1 for Problem 33
add_decision_option_link(problem_33, solution_1_33, "Solution 1")

# Create Problem 28
problem_07 = CClass(
    decision, "Failure to centralize cryptography", stereotype_instances=multiple_answers)

# Create Solution 1 for Problem 
solution_1_07 = CClass(design_solution, "Use Updated and Established Cryptographic Functions, Algorithms, and Protocols ",
                    values={"background reading": "s07"})

# Create Solution 2 for Problem 28
solution_2_07 = CClass(design_solution, " Blockchain-powered cybersecurity controls and standards to defend enterprises against cyberattacks.",
                    values={"background reading": "s07"})

# Link Problem 28 to Solutions for Problem 28
add_decision_option_link(problem_07, solution_1_07, "Solution 1")
add_decision_option_link(problem_07, solution_2_07, "Solution 2")

# Connect Solution 1 for Problem 42 to Problem 28
add_links({solution_1_33: problem_07}, role_name="next decision")

problem_10 = CClass(
    decision, " Poor passwords, Weak passwords, and password reuse ", stereotype_instances=multiple_answers)

solution_1_10 = CClass(design_solution, "Use security testing",
                    values={"background reading": "s10"})
solution_2_10 = CClass(design_solution, "Use a password hashing technique to generate a unique hash of the user's password that could be saved in database",
                    values={"background reading": "s10"})
solution_3_10 = CClass(design_solution, "Using strong passwords that are difficult to crack, ex. At least seven characters long, Contains secret or random information, Is significantly different from previous passwords, Uppercase letters, Lowercase letters, Numerals, and Symbols including spaces ",
                    values={"background reading": "s10"})
solution_4_10= CClass(design_solution, "Maintain good password hygiene",
                    values={"background reading": "s10"})
solution_5_10 = CClass(design_solution, "Weakened Security Measures",
                    values={"background reading": "s10"})


add_decision_option_link(problem_10, solution_1_10, "Solution 1")
add_decision_option_link(problem_10, solution_2_10, "Solution 2")
add_decision_option_link(problem_10, solution_3_10, "Solution 3")
add_decision_option_link(problem_10, solution_4_10, "Solution 4")
add_decision_option_link(problem_10, solution_5_10, "Solution 5")


add_links({solution_1_33: problem_10}, role_name="next decision")

problem_40 = CClass(
    decision, "Fragmented tools and one-off solutions ", stereotype_instances=single_answer)

solution_1_40 = CClass(design_solution, "Adopt a comprehensive security architecture approach for enterprise risk management, avoiding on disconnected tools",
                    values={"background reading": "s40"})

add_decision_option_link(problem_40, solution_1_40, "Solution 1")

add_links({solution_1_07: problem_40}, role_name="next decision")

problem_24 = CClass(
    decision, "Vague requirements", stereotype_instances=single_answer)

solution_1_24 = CClass(design_solution, "Introduce specific requirements focused on incremental steps to improving security ",
                    values={"background reading": "s24"})

add_decision_option_link(problem_24, solution_1_24, "Solution 1")

add_links({solution_1_40: problem_24}, role_name="next decision")

problem_19 = CClass(
    decision, " Legacy software ", stereotype_instances=single_answer)

solution_1_19 = CClass(design_solution, " Make sure to use software that is updated regularly ",
                    values={"background reading": "s19"})

add_decision_option_link(problem_19, solution_1_19, "Solution 1")

add_links({solution_2_07: problem_19}, role_name="next decision")

problem_15 = CClass(
    decision, " Security misconfiguration ", stereotype_instances=single_answer)

solution_1_15 = CClass(design_solution, " Include application security in data privacy compliance strategy ",
                    values={"background reading": "s15"})

add_decision_option_link(problem_15, solution_1_15, "Solution 1")

add_links({solution_1_10: problem_15}, role_name="next decision")

problem_31 = CClass(
    decision, " Frustrations due to inadequate tooling ", stereotype_instances=single_answer)

solution_1_31 = CClass(design_solution, " Avoid depending on SAST tools, as it's vulnerable to triggering several false warnings ",
                    values={"background reading": "s31"})

add_decision_option_link(problem_31, solution_1_31, "Solution 1")

add_links({solution_1_10: problem_31}, role_name="next decision")


_all = CBundle("Diagram2",
                    elements=[problem_33, solution_1_33, problem_07, solution_1_07, solution_2_07, problem_10, solution_1_10, 
                    solution_2_10, solution_3_10, solution_4_10, solution_5_10, problem_40, solution_1_40, problem_24, solution_1_24,
                    problem_19, solution_1_19, problem_15, solution_1_15, problem_31, solution_1_31 ])
 
Diagram2 = [_all, {}]
