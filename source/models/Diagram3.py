from metamodels.guidance_metamodel import decision, design_solution, add_decision_option_link, single_answer, practice, multiple_answers
from codeable_models import CClass, add_links, CBundle

#Problem 4
problem_4 = CClass(decision, "Neglecting to authorize after authentication", stereotype_instances=multiple_answers)

solution_1_4 = CClass(design_solution, "Periodically perform authorization as an explicit check.",
                    values={"background reading": "s01"})
solution_2_4 = CClass(design_solution, "Adopt automated testing.",
                    values={"background reading": "s04"})
solution_3_4 = CClass(design_solution, " Implement strong authentication methods such as multi-factor authentication (MFA) to reduce the risk of unauthorized access.",
                    values={"background reading": "s24"})
solution_4_4 = CClass(design_solution, "Implementing a framework will help you avoid the web security risks caused by faulty authentication.",
                    values={"background reading": "s24"})
solution_5_4 = CClass(design_solution, "Use OAuth2 for single sign on (SSO) with OpenID Connect.",
                    values={"background reading": "s26"})
solution_6_4 = CClass(design_solution, "Add more Authentication ex. Multi-factor authentication, Certificate-based authentication, Biometric authentication, and Token-based authentication.",
                    values={"background reading": "s27"})

add_decision_option_link(problem_4, solution_1_4, "Solution 1")
add_decision_option_link(problem_4, solution_2_4, "Solution 2")
add_decision_option_link(problem_4, solution_3_4, "Solution 3")
add_decision_option_link(problem_4, solution_4_4, "Solution 4")
add_decision_option_link(problem_4, solution_5_4, "Solution 5")
add_decision_option_link(problem_4, solution_6_4, "Solution 6")

#Problem 39
problem_39 = CClass(decision, "Lack of defense in depth", stereotype_instances=single_answer)

solution_1_39 = CClass(design_solution, "Implement multiple layers of security defenses to reduce the likelihood of a successful attack.",
                    values={"background reading": "s18"})

add_decision_option_link(problem_39, solution_1_39, "Solution 1")

#Problem 32
problem_32 = CClass(decision, "Man-in-the-middle (MitM) attacks", stereotype_instances=single_answer)

solution_1_32 = CClass(design_solution, "Properly scope permissions across users and machines.",
                    values={"background reading": "s32"})

add_decision_option_link(problem_32, solution_1_32, "Solution 1")

#Problem 3
problem_3 = CClass(decision, "an unauthorized entity having access to a system or service that it should not", stereotype_instances=multiple_answers)

solution_1_3 = CClass(design_solution, "Implement Principle of Least Privilege.",
                    values={"background reading": "s01"})
solution_2_3 = CClass(design_solution, "Train employees on cyber security best practices.",
                    values={"background reading": "s02"})
solution_3_3 = CClass(design_solution, "Architecture analysis assessments.",
                    values={"background reading": "s10"})

add_decision_option_link(problem_3, solution_1_3, "Solution 1")
add_decision_option_link(problem_3, solution_2_3, "Solution 2")
add_decision_option_link(problem_3, solution_3_3, "Solution 3")

#Link Diagram
add_links({solution_2_4: problem_39}, role_name="next decision")
add_links({solution_3_4: problem_32}, role_name="next decision")
add_links({solution_6_4: problem_3}, role_name="next decision")

_all = CBundle("Diagram3",
                    elements=[problem_4, solution_1_4, solution_2_4, solution_3_4 ,solution_4_4 ,solution_5_4 ,solution_6_4,
                              problem_39 , solution_1_39, 
                              problem_32 , solution_1_32,
                              problem_3 , solution_1_3 , solution_2_3 , solution_3_3
                              ])


Diagram3 = [_all, {}]
