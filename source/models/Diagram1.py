from metamodels.guidance_metamodel import decision, design_solution, add_decision_option_link, single_answer, practice, multiple_answers
from codeable_models import CClass, add_links, CBundle


problem_42 = CClass(
    decision, "Insufficient hardware and software security measures", stereotype_instances=single_answer)

# Create Solution 1 for Problem 42
solution_1_42 = CClass(design_solution, "Implement robust security measures. For example, firewalls, encryption.",
                    values={"background reading": "s42"})

# Link Problem 42 to Solution 1 for Problem 42
add_decision_option_link(problem_42, solution_1_42, "Solution 1")

# Create Problem 28
problem_28 = CClass(
    decision, "Encryption Weakness", stereotype_instances=multiple_answers)

# Create Solution 1 for Problem 28
solution_1_28 = CClass(design_solution, "Plan better protection like encrypting data on USB sticks, laptops, and desktops.",
                    values={"background reading": "s28"})

# Create Solution 2 for Problem 28
solution_2_28 = CClass(design_solution, "All data should be encrypted in transit and at rest. This includes database storage, file storage, sessions, cookies, etc.",
                    values={"background reading": "s28"})

# Link Problem 28 to Solutions for Problem 28
add_decision_option_link(problem_28, solution_1_28, "Solution 1")
add_decision_option_link(problem_28, solution_2_28, "Solution 2")

# Connect Solution 1 for Problem 42 to Problem 28
add_links({solution_1_42: problem_28}, role_name="next decision")

problem_11 = CClass(
    decision, "Inadequate Security Awareness and Training", stereotype_instances=multiple_answers)

solution_1_11 = CClass(design_solution, "Dedicated time to focus on security awareness education and security protocol training.",
                    values={"background reading": "s11"})
solution_2_11 = CClass(design_solution, "Seek advice from security experts to identify security challenges effectively.",
                    values={"background reading": "s11"})
solution_3_11 = CClass(design_solution, "Collaborating in an agile, sprint-based model to defend against cyberattacks.",
                    values={"background reading": "s11"})
solution_4_11 = CClass(design_solution, "Organizations should foster a culture that emphasizes the importance of software security.",
                    values={"background reading": "s11"})
solution_5_11 = CClass(design_solution, "Shift Left and Adopt DevSecOps.",
                    values={"background reading": "s11"})
solution_6_11 = CClass(design_solution, "Strive to achieve agile security. Use modern software supply chain security tools like Legit Security that provides an aggregated security score by product line and development team.",
                    values={"background reading": "s11"})
solution_7_11 = CClass(design_solution, "Consider which items are most applicable to your organization, and then start making necessary adjustments.",
                    values={"background reading": "s11"})

add_decision_option_link(problem_11, solution_1_11, "Solution 1")
add_decision_option_link(problem_11, solution_2_11, "Solution 2")
add_decision_option_link(problem_11, solution_3_11, "Solution 3")
add_decision_option_link(problem_11, solution_4_11, "Solution 4")
add_decision_option_link(problem_11, solution_5_11, "Solution 6")
add_decision_option_link(problem_11, solution_6_11, "Solution 7")
add_decision_option_link(problem_11, solution_7_11, "Solution 8")

add_links({solution_1_28: problem_11}, role_name="next decision")

problem_12 = CClass(
    decision, "Failing to Prioritize the Security Team", stereotype_instances=multiple_answers)

solution_1_12 = CClass(design_solution, "drive a cultural change that makes secure development a non-negotiable priority.",
                    values={"background reading": "s12"})
solution_2_12 = CClass(design_solution, "Better collaboration and cautious coding from all parties involved.",
                    values={"background reading": "s12"})
solution_3_12 = CClass(design_solution, "Require ongoing security training for the development team, and require that all newly developed APIs, microservices, integrations, and applications instrument the required security tests in their CI/CD pipelines.",
                    values={"background reading": "s12"})
solution_4_12 = CClass(design_solution, "Prioritize security and recognize it as an essential aspect of software development.",
                    values={"background reading": "s12"})
solution_5_12 = CClass(design_solution, "Take the time to learns about security.",
                    values={"background reading": "s12"})
solution_6_12 = CClass(design_solution, "Spend time re-framing security procedures so that team members understand why a certain security practice is important.",
                    values={"background reading": "s12"})

add_decision_option_link(problem_12, solution_1_12, "Solution 1")
add_decision_option_link(problem_12, solution_2_12, "Solution 2")
add_decision_option_link(problem_12, solution_3_12, "Solution 3")
add_decision_option_link(problem_12, solution_4_12, "Solution 4")
add_decision_option_link(problem_12, solution_5_12, "Solution 5")
add_decision_option_link(problem_12, solution_6_12, "Solution 6")

add_links({solution_3_11: problem_12}, role_name="next decision")

problem_13 = CClass(
    decision, "Weak backend access controls", stereotype_instances=multiple_answers)

solution_1_13 = CClass(design_solution, "Scan for vulnerabilities regularly.",
                    values={"background reading": "s13"})
solution_2_13 = CClass(design_solution, "Restricted user to only necessary area.",
                    values={"background reading": "s13"})
solution_3_13 = CClass(design_solution, "Restrict access and use a password once saved.",
                    values={"background reading": "s13"})

add_decision_option_link(problem_13, solution_1_13, "Solution 1")
add_decision_option_link(problem_13, solution_2_13, "Solution 2")
add_decision_option_link(problem_13, solution_3_13, "Solution 3")

add_links({solution_2_28: problem_13}, role_name="next decision")

_all = CBundle("Diagram1",
                    elements=[problem_42, solution_1_42, problem_28, solution_1_28, solution_2_28, problem_11, solution_1_11, 
                    solution_2_11, solution_3_11, solution_4_11, solution_6_11, solution_7_11, solution_5_11,
                    solution_1_12, solution_2_12, solution_3_12, solution_4_12, solution_5_12, solution_6_12,
                    solution_1_13, solution_2_13, solution_3_13, problem_12, problem_13])

Diagram1 = [_all, {}]
