@startuml
skinparam monochrome true
skinparam ClassBackgroundColor White
hide empty members
hide circle
skinparam defaultFontName Arial
skinparam defaultFontSize 11
left to right direction
class "<b>Unsanitized Input : Decision</b>" as __1_Unsanitized_Input <<Multiple Answers>> 
class "<b>dedicated time to focus on security awareness</b>\n<b>education and security protocol training. : Design</b>\n<b>Solution</b>" as __2_dedicated_time_to_focus_on_security_awareness_education_and_security_protocol_training_ {
background reading = "s16"
}

class "<b>Implement Principle of Least Privilege. : Design</b>\n<b>Solution</b>" as __3_Implement_Principle_of_Least_Privilege_ {
background reading = "s16"
}

class "<b>Continuous monitoring helps in detecting and responding</b>\n<b>to security incidents in real-time. : Design</b>\n<b>Solution</b>" as __4_Continuous_monitoring_helps_in_detecting_and_responding_to_security_incidents_in_real_time_ {
background reading = "s16"
}

class "<b>User activity tracking, file integrity monitoring,</b>\n<b>and network activity logs are examples of monitoring</b>\n<b>and auditing practices. : Design Solution</b>" as __5_User_activity_tracking__file_integrity_monitoring__and_network_activity_logs_are_examples_of_monitoring_and_auditing_practices_ {
background reading = "s16"
}

class "<b>Use security testing. : Design Solution</b>" as __6_Use_security_testing_ {
background reading = "s16"
}

class "<b>Ransomware Attacks : Decision</b>" as __7_Ransomware_Attacks <<Single Answers>> 
class "<b>Prioritize cybersecurity more in budgeting. : Design</b>\n<b>Solution</b>" as __8_Prioritize_cybersecurity_more_in_budgeting_ {
background reading = "s20"
}

class "<b>Incorrect trust assumptions : Decision</b>" as __9_Incorrect_trust_assumptions <<Single Answers>> 
class "<b>Make sure all data from an untrusted client are</b>\n<b>validated. : Design Solution</b>" as __10_Make_sure_all_data_from_an_untrusted_client_are_validated_ {
background reading = "s01"
}

class "<b>Poorly written code : Decision</b>" as __11_Poorly_written_code <<Multiple Answers>> 
class "<b>Treat Software Security as a Priority Right From</b>\n<b>The Start. : Design Solution</b>" as __12_Treat_Software_Security_as_a_Priority_Right_From_The_Start_ {
background reading = "s14"
}

class "<b>Conduct testing to identify and address coding</b>\n<b>bugs. Implement secure coding practices and perform</b>\n<b>regular code reviews to minimize the introduction</b>\n<b>of vulnerabilities. : Design Solution</b>" as __13_Conduct_testing_to_identify_and_address_coding_bugs__Implement_secure_coding_practices_and_perform_regular_code_reviews_to_minimize_the_introduction_of_vulnerabilities_ {
background reading = "s14"
}

class "<b>Maintain an OSS Security Library Inventory. : Design</b>\n<b>Solution</b>" as __14_Maintain_an_OSS_Security_Library_Inventory_ {
background reading = "s14"
}

class "<b>Verify that the code is efficient, scalable and</b>\n<b>secure. : Design Solution</b>" as __15_Verify_that_the_code_is_efficient__scalable_and_secure_ {
background reading = "s14"
}

class "<b>Security misconfiguration : Decision</b>" as __16_Security_misconfiguration <<Single Answers>> 
class "<b>Include application security in data privacy compliance</b>\n<b>strategy. : Design Solution</b>" as __17_Include_application_security_in_data_privacy_compliance_strategy_ {
background reading = "s15"
}

class "<b>Unfettered access to source code repositories</b>\n<b>and CI/CD pipelines : Decision</b>" as __18_Unfettered_access_to_source_code_repositories_and_CI_CD_pipelines <<Multiple Answers>> 
class "<b>Implement automated security testing, avoid access</b>\n<b>to repositories, and apply the principle of least</b>\n<b>privilege. : Design Solution</b>" as __19_Implement_automated_security_testing__avoid_access_to_repositories__and_apply_the_principle_of_least_privilege_ {
background reading = "s17"
}

class "<b>Set limitations or privacy to access source code.</b>\n<b>: Design Solution</b>" as __20_Set_limitations_or_privacy_to_access_source_code_ {
background reading = "s17"
}

class "<b>Use static analytical methods to design and test</b>\n<b>components. : Design Solution</b>" as __21_Use_static_analytical_methods_to_design_and_test_components_ {
background reading = "s17"
}

class "<b>Insecure APIs : Decision</b>" as __22_Insecure_APIs <<Multiple Answers>> 
class "<b>Establishing a policy on open-source use. : Design</b>\n<b>Solution</b>" as __23_Establishing_a_policy_on_open_source_use_ {
background reading = "s09"
}

class "<b>Scan for vulnerabilities regularly. : Design Solution</b>" as __24_Scan_for_vulnerabilities_regularly_ {
background reading = "s09"
}

class "<b>Code review involves reviewing the code written</b>\n<b>by developers to identify potential security</b>\n<b>issues. : Design Solution</b>" as __25_Code_review_involves_reviewing_the_code_written_by_developers_to_identify_potential_security_issues_ {
background reading = "s09"
}

class "<b>Reducing misconfigurations and monitoring for</b>\n<b>security issues. : Design Solution</b>" as __26_Reducing_misconfigurations_and_monitoring_for_security_issues_ {
background reading = "s09"
}

class "<b>Use the services that focus on checking codebase</b>\n<b>dependencies. : Design Solution</b>" as __27_Use_the_services_that_focus_on_checking_codebase_dependencies_ {
background reading = "s09"
}

class "<b>Integrating a scanning tool like Software Composition</b>\n<b>Analysis. : Design Solution</b>" as __28_Integrating_a_scanning_tool_like_Software_Composition_Analysis_ {
background reading = "s09"
}

class "<b>Implement multi-factor authentication. : Design</b>\n<b>Solution</b>" as __29_Implement_multi_factor_authentication_ {
background reading = "s09"
}

class "<b>SQL Injection : Decision</b>" as __30_SQL_Injection <<Multiple Answers>> 
class "<b>Using parameterized queries instead of dynamic</b>\n<b>SQL statements. : Design Solution</b>" as __31_Using_parameterized_queries_instead_of_dynamic_SQL_statements_ {
background reading = "s02"
}

class "<b>Using a web application firewall. : Design Solution</b>" as __32_Using_a_web_application_firewall_ {
background reading = "s02"
}

class "<b>Don’t use dynamic SQL. : Design Solution</b>" as __33_Don_t_use_dynamic_SQL_ {
background reading = "s02"
}

class "<b>Poor key management : Decision</b>" as __34_Poor_key_management <<Multiple Answers>> 
class "<b>Strict policy-based controls to prevent the misuse/reuse</b>\n<b>of keys. : Design Solution</b>" as __35_Strict_policy_based_controls_to_prevent_the_misuse_reuse_of_keys_ {
background reading = "s06"
}

class "<b>Secure coding guidelines and standards. : Design</b>\n<b>Solution</b>" as __36_Secure_coding_guidelines_and_standards_ {
background reading = "s06"
}

class "<b>Fault is irrelevant : Decision</b>" as __37_Fault_is_irrelevant <<Multiple Answers>> 
class "<b>Security teams should focus on vulnerabilities</b>\n<b>by evaluating their scope, managing the risks.</b>\n<b>: Design Solution</b>" as __38_Security_teams_should_focus_on_vulnerabilities_by_evaluating_their_scope__managing_the_risks_ {
background reading = "s23"
}

class "<b>Running automatic test cases to detect difficult</b>\n<b>risks. : Design Solution</b>" as __39_Running_automatic_test_cases_to_detect_difficult_risks_ {
background reading = "s23"
}

class "<b>Unsanitized Input : Decision</b>" as __40_Unsanitized_Input <<Single Answers>> 
class "<b>Avoid placing user-provided input directly into</b>\n<b>SQL statements. : Design Solution</b>" as __41_Avoid_placing_user_provided_input_directly_into_SQL_statements_ {
background reading = "s35"
}

class "<b>Unpatched vulnerabilities : Decision</b>" as __42_Unpatched_vulnerabilities <<Multiple Answers>> 
class "<b>Change to HTTPS, That provides protection against</b>\n<b>these vulnerabilities by encrypting all exchanges</b>\n<b>between a web browser and web server. As a result,</b>\n<b>HTTPS ensures that no one can tamper with these</b>\n<b>transactions, thus securing users' privacy and</b>\n<b>preventing sensitive information from falling</b>\n<b>into the wrong hands. : Design Solution</b>" as __43_Change_to_HTTPS__That_provides_protection_against_these_vulnerabilities_by_encrypting_all_exchanges_between_a_web_browser_and_web_server__As_a_result__HTTPS_ensures_that_no_one_can_tamper_with_these_transactions__thus_securing_users__privacy_and_preventing_sensitive_information_from_falling_into_the_wrong_hands_ {
background reading = "s38"
}

class "<b>Train employees on cyber security best practices.</b>\n<b>: Design Solution</b>" as __44_Train_employees_on_cyber_security_best_practices_ {
background reading = "s38"
}

__1_Unsanitized_Input --> __2_dedicated_time_to_focus_on_security_awareness_education_and_security_protocol_training_: <<Option>>\n{name = "Solution 1"}
__1_Unsanitized_Input --> __3_Implement_Principle_of_Least_Privilege_: <<Option>>\n{name = "Solution 2"}
__1_Unsanitized_Input --> __4_Continuous_monitoring_helps_in_detecting_and_responding_to_security_incidents_in_real_time_: <<Option>>\n{name = "Solution 3"}
__1_Unsanitized_Input --> __5_User_activity_tracking__file_integrity_monitoring__and_network_activity_logs_are_examples_of_monitoring_and_auditing_practices_: <<Option>>\n{name = "Solution 4"}
__1_Unsanitized_Input --> __6_Use_security_testing_: <<Option>>\n{name = "Solution 5"}
__7_Ransomware_Attacks --> __8_Prioritize_cybersecurity_more_in_budgeting_: <<Option>>\n{name = "Solution 1"}
__9_Incorrect_trust_assumptions --> __10_Make_sure_all_data_from_an_untrusted_client_are_validated_: <<Option>>\n{name = "Solution 1"}
__10_Make_sure_all_data_from_an_untrusted_client_are_validated_ --> __40_Unsanitized_Input
__10_Make_sure_all_data_from_an_untrusted_client_are_validated_ --> __1_Unsanitized_Input
__13_Conduct_testing_to_identify_and_address_coding_bugs__Implement_secure_coding_practices_and_perform_regular_code_reviews_to_minimize_the_introduction_of_vulnerabilities_ --> __18_Unfettered_access_to_source_code_repositories_and_CI_CD_pipelines
__16_Security_misconfiguration --> __17_Include_application_security_in_data_privacy_compliance_strategy_: <<Option>>\n{name = "Solution 1"}
__18_Unfettered_access_to_source_code_repositories_and_CI_CD_pipelines --> __19_Implement_automated_security_testing__avoid_access_to_repositories__and_apply_the_principle_of_least_privilege_: <<Option>>\n{name = "Solution 1"}
__18_Unfettered_access_to_source_code_repositories_and_CI_CD_pipelines --> __20_Set_limitations_or_privacy_to_access_source_code_: <<Option>>\n{name = "Solution 2"}
__18_Unfettered_access_to_source_code_repositories_and_CI_CD_pipelines --> __21_Use_static_analytical_methods_to_design_and_test_components_: <<Option>>\n{name = "Solution 3"}
__22_Insecure_APIs --> __23_Establishing_a_policy_on_open_source_use_: <<Option>>\n{name = "Solution 1"}
__22_Insecure_APIs --> __24_Scan_for_vulnerabilities_regularly_: <<Option>>\n{name = "Solution 2"}
__22_Insecure_APIs --> __25_Code_review_involves_reviewing_the_code_written_by_developers_to_identify_potential_security_issues_: <<Option>>\n{name = "Solution 3"}
__22_Insecure_APIs --> __26_Reducing_misconfigurations_and_monitoring_for_security_issues_: <<Option>>\n{name = "Solution 4"}
__22_Insecure_APIs --> __27_Use_the_services_that_focus_on_checking_codebase_dependencies_: <<Option>>\n{name = "Solution 5"}
__22_Insecure_APIs --> __28_Integrating_a_scanning_tool_like_Software_Composition_Analysis_: <<Option>>\n{name = "Solution 6"}
__22_Insecure_APIs --> __29_Implement_multi_factor_authentication_: <<Option>>\n{name = "Solution 7"}
__23_Establishing_a_policy_on_open_source_use_ --> __42_Unpatched_vulnerabilities
__24_Scan_for_vulnerabilities_regularly_ --> __37_Fault_is_irrelevant
__25_Code_review_involves_reviewing_the_code_written_by_developers_to_identify_potential_security_issues_ --> __11_Poorly_written_code
__26_Reducing_misconfigurations_and_monitoring_for_security_issues_ --> __16_Security_misconfiguration
__27_Use_the_services_that_focus_on_checking_codebase_dependencies_ --> __9_Incorrect_trust_assumptions
__30_SQL_Injection --> __31_Using_parameterized_queries_instead_of_dynamic_SQL_statements_: <<Option>>\n{name = "Solution 1"}
__30_SQL_Injection --> __32_Using_a_web_application_firewall_: <<Option>>\n{name = "Solution 2"}
__30_SQL_Injection --> __33_Don_t_use_dynamic_SQL_: <<Option>>\n{name = "Solution 3"}
__32_Using_a_web_application_firewall_ --> __34_Poor_key_management
__32_Using_a_web_application_firewall_ --> __22_Insecure_APIs
__34_Poor_key_management --> __35_Strict_policy_based_controls_to_prevent_the_misuse_reuse_of_keys_: <<Option>>\n{name = "Solution 1"}
__34_Poor_key_management --> __36_Secure_coding_guidelines_and_standards_: <<Option>>\n{name = "Solution 2"}
__37_Fault_is_irrelevant --> __38_Security_teams_should_focus_on_vulnerabilities_by_evaluating_their_scope__managing_the_risks_: <<Option>>\n{name = "Solution 1"}
__37_Fault_is_irrelevant --> __39_Running_automatic_test_cases_to_detect_difficult_risks_: <<Option>>\n{name = "Solution 2"}
__37_Fault_is_irrelevant --> __12_Treat_Software_Security_as_a_Priority_Right_From_The_Start_: <<Option>>\n{name = "Solution 1"}
__37_Fault_is_irrelevant --> __13_Conduct_testing_to_identify_and_address_coding_bugs__Implement_secure_coding_practices_and_perform_regular_code_reviews_to_minimize_the_introduction_of_vulnerabilities_: <<Option>>\n{name = "Solution 2"}
__37_Fault_is_irrelevant --> __14_Maintain_an_OSS_Security_Library_Inventory_: <<Option>>\n{name = "Solution 3"}
__37_Fault_is_irrelevant --> __15_Verify_that_the_code_is_efficient__scalable_and_secure_: <<Option>>\n{name = "Solution 4"}
__38_Security_teams_should_focus_on_vulnerabilities_by_evaluating_their_scope__managing_the_risks_ --> __42_Unpatched_vulnerabilities
__40_Unsanitized_Input --> __41_Avoid_placing_user_provided_input_directly_into_SQL_statements_: <<Option>>\n{name = "Solution 1"}
__42_Unpatched_vulnerabilities --> __43_Change_to_HTTPS__That_provides_protection_against_these_vulnerabilities_by_encrypting_all_exchanges_between_a_web_browser_and_web_server__As_a_result__HTTPS_ensures_that_no_one_can_tamper_with_these_transactions__thus_securing_users__privacy_and_preventing_sensitive_information_from_falling_into_the_wrong_hands_: <<Option>>\n{name = "Solution 1"}
__42_Unpatched_vulnerabilities --> __44_Train_employees_on_cyber_security_best_practices_: <<Option>>\n{name = "Solution 2"}
__43_Change_to_HTTPS__That_provides_protection_against_these_vulnerabilities_by_encrypting_all_exchanges_between_a_web_browser_and_web_server__As_a_result__HTTPS_ensures_that_no_one_can_tamper_with_these_transactions__thus_securing_users__privacy_and_preventing_sensitive_information_from_falling_into_the_wrong_hands_ --> __7_Ransomware_Attacks
@enduml
